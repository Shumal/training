{ 
    'name': 'To-Do App',
    'description': 'Manage your personal To-Do tasks.', 
    'author': 'Daniel Reis', 
    'depends': ['base'], 
    'application': True,

    'data': [
        'security/todo_security.xml',
        'security/ir.model.access.csv',
    	'views/todo_menu.xml',
    	'views/todo_view.xml'
    ],
}
